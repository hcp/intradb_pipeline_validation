 /**
 * Copyright (c) 2014 Washington University School of Medicine
 */

package org.nrg.validation;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xnattools.SessionManager;

/**
 * @author Mohana Ramaratnam (mohanakannan9@gmail.com)
 *
 *
 */
public class HCPShimValidator extends HCPSessionValidator {

	private final static String LOCALIZER_TYPE="LOCALIZER";
	private final static String SBREF="_SBRef";
	
	/*
	 * This method will validate if the SHIM values are set correctly. 
	 * Each time a Localizer is encountered, that is an indication that a possibility
	 * is there that the shim was re-adjusted. The reference scan to use to check for
	 * correctness of the SHIM is the first *_SBREF scan after the Localizer.
	 * For the entire set of MOSAIC acquisitions between the location of the reference FMRI scan
	 * and the next localizer scan, the SHIM must be the same.
	 */
	private static boolean IsShimValid(XnatImagesessiondataBean imageSession) {
		boolean isValid = true;
		//Assumption - scan ids are in temporal order
		List<XnatImagescandataI> scans_in_temporal_order = SortByAcquisitionTime(imageSession);
		if (scans_in_temporal_order == null || scans_in_temporal_order.size() == 0) {
			return !isValid;
		}
		Hashtable<Integer, Integer> shimmedFRMIBlockIndices=GetReferenceFMRIBlockIndices(scans_in_temporal_order);
		Set<Integer> startIndexKeys = shimmedFRMIBlockIndices.keySet();
		Iterator<Integer> keyIterator = startIndexKeys.iterator(); 
		while(keyIterator.hasNext()){
			Integer startIndex = keyIterator.next();
			Integer endIndex = shimmedFRMIBlockIndices.get(startIndex);
			isValid = CheckShimCurrentInTheBlock(startIndex,endIndex, scans_in_temporal_order);
			System.out.println("Start " + startIndex + " End:" + endIndex + " " + isValid);
			if (!isValid) {
				break;
			}
		}
		return isValid;
	}
     
	private static Hashtable<Integer, Integer> GetReferenceFMRIBlockIndices(List<XnatImagescandataI> scans) {
		Hashtable<Integer, Integer> referenceFMRIBlocks = new Hashtable<Integer, Integer>();
		ArrayList<Integer> startFMRIIndices = new ArrayList<Integer>();
		ArrayList<Integer> localizerIndices = new ArrayList<Integer>();

		if (scans != null && scans.size() > 0) {
			int i=0;
			while (i<scans.size()) {
				if (scans.get(i).getType().equalsIgnoreCase(LOCALIZER_TYPE)) {
					localizerIndices.add(i);
					//Look ahead for the nearest FMRI SBRef scan
					for (int j=(i+1); j<scans.size(); j++) {
						XnatMrscandataBean mrScan = (XnatMrscandataBean)scans.get(j);
						if (mrScan.getParameters_imagetype().toUpperCase().endsWith(MOSAIC_TYPE) && mrScan.getType().endsWith(SBREF)) {
							startFMRIIndices.add(new Integer(j));
							//Skip intermediate scans
							i=j;
							break;
						}
					}
				}
				i++;
			}
			/*
			 * Number of reference Blocks would be same as the number of localizer scans
			 */
			if (localizerIndices.size() == 1) {
				//There was only one localizer. Good Imaging session!
				if (startFMRIIndices.size() > 0) {
					referenceFMRIBlocks.put(new Integer(startFMRIIndices.get(0)), new Integer(scans.size()));
				}
			}else {
				//More than one localizer. Possibility of subject adjustment
				for (i=0; i<localizerIndices.size(); i++) {
					try {
						int endIndex=0;
						if ((i+1)<localizerIndices.size()) {
							endIndex=localizerIndices.get(i+1);
						}else{
							endIndex=scans.size();
						}
						referenceFMRIBlocks.put(new Integer(startFMRIIndices.get(i)), new Integer(endIndex));
					}catch(IndexOutOfBoundsException iobe){
						iobe.printStackTrace();
					}
				}
			}
		}
		return referenceFMRIBlocks;
	}
	
	public static boolean CheckShim(String host, String username, String password, String xnatId) {
		boolean result = false;
    	SessionManager sessionManager = SessionManager.GetInstance(host, username, password);
		XnatImagesessiondataBean imageSession = GetImageSession(host,username,password,xnatId);
		result = IsShimValid(imageSession);
		try {
			sessionManager.deleteJSESSION();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static boolean CheckShim(String pathToXML) {
		boolean result = false;
		try {
			XnatImagesessiondataBean imageSession = GetImageSession(pathToXML);
			result = IsShimValid(imageSession);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	
	public static void main(String args[]) {
		String xmlPath=args[0];
		boolean shimCheck = CheckShim(xmlPath);
		System.out.println("Shim Check " + shimCheck);
		System.exit(0);
	}
	
 
	
}
