 /**
 * Copyright (c) 2015 Washington University School of Medicine
 */

package org.nrg.validation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.nrg.xdat.model.XnatAddfieldI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xnattools.SessionManager;

/**
 * @author Mohana Ramaratnam (mohanakannan9@gmail.com)
 *
 *
 */
public class LS3T7TShimValidator extends HCPSessionValidator {

	private final static String LOCALIZER_TYPE="LOCALIZER";
	private final static String SBREF_3T="FieldMap_SE_EPI";
	private final static String SBREF="_SBRef";
	private final static String suffix_7T="_7T";
	private final static String suffix_3T="_3T";
	private final static String T1W_SCAN_TYPE="T1w";
	private final static String T2W_SCAN_TYPE="T2w";
	private final static String DIFFUSION_SCAN_TYPE="dMRI";
	private final static String FIELDMAP_DIFFUSION_SCAN_TYPE="FieldMap_diffusion";
	private final static String SPINECHOFIELDMAP_TYPE="FieldMap_SE_EPI";
	private final static String RFMRI = "rfMRI";
	private final static String SHIM_CURRENT = "Siemens GRADSPEC alShimCurrent";
	private final static String SHIM_CURRENT_OFFSET = "Siemens GRADSPEC lOffset";
	private final static double FIELDMAP_DIFFUSION_OFFSET_LIMIT=2.176 ; //1.55+0.626
	private final static double DIFFUSION_OFFSET_LIMIT=2.801; //2.175 + 0.626

	
	private static String appropriate_sbref="";
	private static Hashtable<String, Integer> scanTypesToSkipGlobalPositionCheck;
	private static boolean is7T = false;
	private static ArrayList<String> basicScanParamNames;
	private static ArrayList<String> normalScanParamNames;
	private static ArrayList<String> positionScanParamNames;
	private static Hashtable<String, Integer> scanTypesToCheck;
	
	
	private  static void init() {
		scanTypesToCheck = new Hashtable<String,Integer>();
		scanTypesToCheck.put(SPINECHOFIELDMAP_TYPE,1);
		scanTypesToCheck.put(RFMRI,1);
		scanTypesToCheck.put(T1W_SCAN_TYPE,1);
		scanTypesToCheck.put(T2W_SCAN_TYPE,1);
		scanTypesToCheck.put(DIFFUSION_SCAN_TYPE,1);

	}
	/*
	 * This method will validate if the SHIM values are set correctly.
	 * Each time a Localizer is encountered, that is an indication that a possibility
	 * is there that the shim was re-adjusted. The reference scan to use to check for
	 * correctness of the SHIM is the first scan after the Localizer for which the SHIM changes.
	 * For the entire set of MOSAIC acquisitions between the location of the reference FMRI scan
	 * and the next localizer scan, the SHIM must be the same.
	 */
	private static boolean IsShimValid(XnatImagesessiondataBean imageSession) {
		boolean isValid = true;
		if (is7T) {
			appropriate_sbref = SBREF;
		}else {
			appropriate_sbref = SBREF_3T;
		}
	   basicScanParamNames = new ArrayList<String>(
	   Arrays.asList("Siemens GRADSPEC alShimCurrent","Siemens GRADSPEC lOffset", "Siemens txRefAmp"));
	   normalScanParamNames = new ArrayList<String>(
	   Arrays.asList("Siemens sSliceArray.asSlice[0].sNormal.dSag","Siemens sSliceArray.asSlice[0].sNormal.dCor","Siemens sSliceArray.asSlice[0].sNormal.dTra"));
	   positionScanParamNames = new ArrayList<String>(
	   Arrays.asList("Siemens sSliceArray.asSlice[0].sPosition.dSag","Siemens sSliceArray.asSlice[0].sPosition.dCor","Siemens sSliceArray.asSlice[0].sPosition.dTra"));

		//Assumption - scan ids are in temporal order
		List<XnatImagescandataI> scans_in_temporal_order = SortByAcquisitionTime(imageSession);
		if (scans_in_temporal_order == null || scans_in_temporal_order.size() == 0) {
			return !isValid;
		}
		Hashtable<Integer, Integer> shimmedFRMIBlockIndices=GetReferenceFMRIBlockIndices(scans_in_temporal_order);
		Set<Integer> startIndexKeys = shimmedFRMIBlockIndices.keySet();
		Iterator<Integer> keyIterator = startIndexKeys.iterator();
		while(keyIterator.hasNext()){
			Integer startIndex = keyIterator.next();
			Integer endIndex = shimmedFRMIBlockIndices.get(startIndex);
			isValid = LS3T7TShimValidator.CheckShimCurrentInTheBlock(startIndex,endIndex, scans_in_temporal_order);
			Integer firstReferenceScanIndex = LS3T7TShimValidator.GetFirstReferenceScanInBlock(startIndex,endIndex, scans_in_temporal_order);
			//if (endIndex==scans_in_temporal_order.size()) {
				///endIndex--;
			//}
			try {
				System.out.println("Shim BLOCK - Start Scan ID " + scans_in_temporal_order.get(startIndex).getId() + " End Scan ID " + scans_in_temporal_order.get(endIndex).getId() );
			}catch(Exception e){
				System.out.println("Shim BLOCK - Start Scan ID " + scans_in_temporal_order.get(startIndex).getId() + " End Scan ID " + scans_in_temporal_order.get(endIndex-1).getId() );
			}
			if (!isValid) {
				break;
			}
			//Now Check the  values for other scan types within BLOCK
			if (is7T) {
				boolean areDiffusionAcquiredCorrectly = CheckDiffusionAcquisitions(scans_in_temporal_order,firstReferenceScanIndex,startIndex,endIndex);		
				isValid = (isValid && areDiffusionAcquiredCorrectly);
			}else {
				boolean areT1T2AcquiredCorrectly = CheckT1T2Acquisitions(scans_in_temporal_order,startIndex,endIndex);
				boolean areDiffusionAcquiredCorrectly = CheckDiffusionAcquisitions(scans_in_temporal_order,firstReferenceScanIndex,startIndex,endIndex);		
				isValid =(isValid && areT1T2AcquiredCorrectly && areDiffusionAcquiredCorrectly);
			}
			if (!isValid) {
				break;
			}
		}
		return isValid;
	}

	
	private static Integer GetFirstReferenceScanInBlock(Integer startIndexInt, Integer endIndexInt, List<XnatImagescandataI> scans) {
	   Integer referenceIndex = null;
	   int startIndex = startIndexInt.intValue();
	   int endIndex = endIndexInt.intValue();

		for (int i = startIndex; i < endIndex; i++ ) {
			   XnatMrscandataBean aScan = (XnatMrscandataBean)scans.get(i);
			   if (SPINECHOFIELDMAP_TYPE.equals(aScan.getType()) || RFMRI.equals(aScan.getType())) {
				   referenceIndex = new Integer(i);
				   break;
			   }
		}
		return referenceIndex;
	}
	

	private static boolean CheckT1T2Acquisitions(List<XnatImagescandataI> scans, Integer startIndexInt, Integer endIndexInt) {
		boolean isValid = true;
		Hashtable<String,String> scanTypes=new Hashtable<String,String>() {{
		  put(T1W_SCAN_TYPE,"1");
		  put(T2W_SCAN_TYPE,"1");
		}};
		List<XnatImagescandataI> t1t2_scans = GetScansByType(scans,startIndexInt, endIndexInt,scanTypes);
		if (t1t2_scans != null && t1t2_scans.size() > 0) {
		   //All sPosition and sNormal must match
			XnatMrscandataBean reference = (XnatMrscandataBean)t1t2_scans.get(0);
 		   String ref_normals = GetParameterValues(reference,normalScanParamNames);
		   String ref_position = GetParameterValues(reference,positionScanParamNames);
   		   String reference_compare=ref_normals + ref_position;

		   for (int i=1; i<t1t2_scans.size(); i++) {
				XnatMrscandataBean aScan = (XnatMrscandataBean)t1t2_scans.get(i);
		 		String scan_normals = GetParameterValues(aScan,normalScanParamNames);
				String scan_position = GetParameterValues(aScan,positionScanParamNames);
		   		String scan_compare = scan_normals + scan_position;
		   		   if (!reference_compare.equals(scan_compare)) {
		   			   isValid = false;
		   			   System.out.println("Scan " +aScan.getId() + " sPosition/sNormal values " + scan_compare + " does not match Scan " + reference.getId() + " sPosition/sNormal values " + reference_compare);
		   			   break;
		   		   }
		   }
		}
		if (isValid) {
			System.out.println("Passes T1w/T2w normal and position check");
		}else {
			System.out.println("Fails T1w/T2w normal and position check");
		}	
		return isValid;
	}

	
	private static boolean CheckDiffusionAcquisitions(List<XnatImagescandataI> scans, Integer referenceScanIndexInBlock, Integer startIndexInt, Integer endIndexInt) {
		Hashtable<String,String> scanTypes;
		scanTypes=new Hashtable<String,String>() {{
			  put(DIFFUSION_SCAN_TYPE,"1");
		}};
		boolean areDiffValid = CheckDiffusionAcquisitions(scans,referenceScanIndexInBlock, startIndexInt,endIndexInt,scanTypes, DIFFUSION_OFFSET_LIMIT) ;
		if (is7T) {
			scanTypes=new Hashtable<String,String>() {{
				  put(FIELDMAP_DIFFUSION_SCAN_TYPE,"1");
			}};	
			boolean isFieldMapValid = CheckDiffusionAcquisitions(scans,referenceScanIndexInBlock, startIndexInt,endIndexInt,scanTypes,FIELDMAP_DIFFUSION_OFFSET_LIMIT ) ;
			return areDiffValid && isFieldMapValid; 
		}else
			return areDiffValid; 
	}
	
	private static boolean CheckDiffusionAcquisitions(List<XnatImagescandataI> scans, Integer referenceScanIndexInBlock, Integer startIndexInt, Integer endIndexInt,Hashtable<String,String> scanTypes, double tolerance ) {
		boolean isValid = true;
		XnatMrscandataBean referenceAValueScan = (XnatMrscandataBean)scans.get(referenceScanIndexInBlock);
		List<XnatImagescandataI> diff_scans = GetScansByType(scans,startIndexInt, endIndexInt,scanTypes);
		if (diff_scans != null && diff_scans.size() > 0) {
		   //All sPosition and sNormal must match
		   for (int i=0; i<diff_scans.size(); i++) {
				XnatMrscandataBean aScan = (XnatMrscandataBean)diff_scans.get(i);
		   		double offset = calculateDistance(referenceAValueScan, aScan);
			   	   System.out.println("Offset " + offset + " Scan " + aScan.getId() + " reference " + referenceAValueScan.getId() + " tolerance="+tolerance);

		   		if (offset>=tolerance) {
		   			   isValid=false;
				   	   System.out.println("Offset " + offset + " Scan " + aScan.getId() + " reference " + referenceAValueScan.getId() + " tolerance="+tolerance);
		   			   break;
				}
		   }
			if (isValid) {
				System.out.println("Passes " + scanTypes.toString() +"position check");
			}else {
				System.out.println("Fails dMRI position check");
			}	
		}
		return isValid;
	}

	private static double calculateDistance(XnatMrscandataBean referenceScan,XnatMrscandataBean aScan) {
			double sp0=new Double(GetParameterValue(aScan,"Siemens sSliceArray.asSlice[0].sPosition.dSag")).doubleValue();
			double sp1=new Double(GetParameterValue(aScan,"Siemens sSliceArray.asSlice[0].sPosition.dCor")).doubleValue();
			double sp2=new Double(GetParameterValue(aScan,"Siemens sSliceArray.asSlice[0].sPosition.dTra")).doubleValue();
			double rsp0=new Double(GetParameterValue(referenceScan,"Siemens sSliceArray.asSlice[0].sPosition.dSag")).doubleValue();
			double rsp1=new Double(GetParameterValue(referenceScan,"Siemens sSliceArray.asSlice[0].sPosition.dCor")).doubleValue();
			double rsp2=new Double(GetParameterValue(referenceScan,"Siemens sSliceArray.asSlice[0].sPosition.dTra")).doubleValue();
			double offset=Math.sqrt((rsp0-sp0)*(rsp0-sp0)+(rsp1-sp1)*(rsp1-sp1)+(rsp2-sp2)*(rsp2-sp2));
			return  offset;
	}
		

	
	private static List<XnatImagescandataI> GetScansByType(XnatImagesessiondataBean imageSession, Hashtable<String,String> scanTypes) {
		List<XnatImagescandataI> scans = new ArrayList<XnatImagescandataI>();
		List<XnatImagescandataI> image_scans = imageSession.getScans_scan();
		if (image_scans != null && image_scans.size() > 0) {
			for (int i=0; i<image_scans.size(); i++) {
				XnatImagescandataI aScan = image_scans.get(i);
				if (scanTypes.containsKey(aScan.getType())) {
					scans.add(aScan);
				}
			}
		}	
		return scans;
	}

	private static List<XnatImagescandataI> GetScansByType(List<XnatImagescandataI> scans,  Integer startIndexInt, Integer endIndexInt, Hashtable<String,String> scanTypes) {
		List<XnatImagescandataI> scanByTypes = new ArrayList<XnatImagescandataI>();
		if (scans != null && scans.size() > 0) {
			for (int i=startIndexInt.intValue(); i<endIndexInt.intValue(); i++) {
				XnatImagescandataI aScan = scans.get(i);
				if (scanTypes.containsKey(aScan.getType())) {
					scanByTypes.add(aScan);
				}
			}
		}	
		return scanByTypes;
	}
	
	
	/**
	 * Overloaded - HCP Has same sPosition but LS3T7T does not.
	 * 
	 * Ideal 3T:
 	 	alShimCurrent	sPosition	txRefAmp	sNormal
1	Localizer	-	-	-	-
2	SE Fieldmap AP	A	A	A	A
3	SE Fieldmap PA	A	A	A	A
4	BOLD AP	A	A	A	A
5	BOLD PA	A	A	A	A
6	T1w	A	B	A	B
7	T2w	A	B	A	B
8	Diff AP	A	C	A	A
9	Diff PA	A	C	A	A
10	SE Fieldmap AP	A	A	A	A
11	SE Fieldmap PA	A	A	A	A
12	BOLD PA	A	A	A	A
13	BOLD AP	A	A	A	A
14	Diff AP	A	C	A	A
15	Diff PA	A	C	A	A 


Ideal 7T:


alShimCurrent	sPosition	txRefAmp	sNormal
1	Localizer	-	-	-	-
2	SE Fieldmap AP	A	A	A	A
3	SE Fieldmap PA	A	A	A	A
4	BOLD AP	A	A	A	A
5	BOLD PA	A	A	A	A
6	Diff AP	A	B	A	A
7	Diff PA	A	B	A	A
8	SE Fieldmap AP	A	A	A	A
9	SE Fieldmap PA	A	A	A	A
10	BOLD PA	A	A	A	A
11	BOLD AP	A	A	A	A
12	Diff AP	A	B	A	A
13	Diff PA	A	B	A	A
14	GRE Fieldmap	A	C	A	A #Update On March 30, 2016

	 **/
	protected static boolean CheckShimCurrentInTheBlock(Integer startIndexInt, Integer endIndexInt, List<XnatImagescandataI> scans) {
		   boolean isValidShim = true;
		   int startIndex = startIndexInt.intValue();
		   int endIndex = endIndexInt.intValue();
		   try {
			   XnatMrscandataBean referenceFMRIScan = (XnatMrscandataBean)scans.get(startIndex);
			   for (int i=startIndex+1; i<endIndex; i++) {
				   boolean matched = CheckShimAgainstReference(referenceFMRIScan,(XnatMrscandataBean)scans.get(i));
				   if (!matched) {
					   System.out.println(" Shim checks for scan " + referenceFMRIScan.getId() + " doesnt match " + scans.get(i).getId());
					   isValidShim=false;
					   break;
				   }
			   }
		   }catch(IndexOutOfBoundsException iobe) {
			   isValidShim=false;
		   }
		   return isValidShim;
	   }

	
	   private static  boolean CheckShimAgainstReference(XnatMrscandataBean reference, XnatMrscandataBean aScan) {
		   boolean rtn = false;
		   if (!scanTypesToCheck.containsKey(aScan.getType())) {
			   System.out.println("Skipping " + aScan.getId() + " " + aScan.getType());
			   return true;
		   }
		   String ref_shim_refAmp = GetParameterValues(reference,basicScanParamNames);
 		   String ref_normals = GetParameterValues(reference,normalScanParamNames);
		   String ref_position = GetParameterValues(reference,positionScanParamNames);

		   String scan_shim_refAmp = GetParameterValues(aScan,basicScanParamNames);		   
		   String scan_normals = GetParameterValues(aScan,normalScanParamNames);
		   String scan_position = GetParameterValues(aScan,positionScanParamNames);
		   
		   if (scanTypesToSkipGlobalPositionCheck.containsKey(aScan.getType())) {
			   if (is7T) {
		   		   String reference_compare=ref_shim_refAmp + ref_normals;
		   		   String scan_compare=scan_shim_refAmp + scan_normals;
		   		   if (reference_compare.equals(scan_compare)) {
		   			   rtn =true;
		   		   }
			   }else {
				   if (aScan.getType().equalsIgnoreCase(T1W_SCAN_TYPE) || aScan.getType().equalsIgnoreCase(T2W_SCAN_TYPE)) {
					   if (ref_shim_refAmp.equals(scan_shim_refAmp)) {
						   rtn = true;
					   }
				   }else {
			   		   String reference_compare=ref_shim_refAmp + ref_normals;
			   		   String scan_compare=scan_shim_refAmp + scan_normals;
			   		   if (reference_compare.equals(scan_compare)) {
			   			   rtn =true;
			   		   }
				   }
			   }
		   } else {
	   		   String reference_compare=ref_shim_refAmp + ref_normals + ref_position;
	   		   String scan_compare=scan_shim_refAmp + scan_normals + ref_position;
	   		   if (reference_compare.equals(scan_compare)) {
	   			   rtn =true;
	   		   }
		   }
		   if (!rtn) {
			   System.out.println("Reference="+reference.getId() + " Scan=" +aScan.getId());
			   System.out.println("Reference");
			   System.out.println(ref_shim_refAmp + " Normal=" + ref_normals + " Position=" + ref_position);
			   System.out.println("Scan");
			   System.out.println(scan_shim_refAmp + " Normal=" + scan_normals + " Position=" + scan_position);

		   }
		   return rtn;
	   }
	
	   private static String GetParameterValues(XnatMrscandataBean scan, ArrayList<String> paramNames) {
		   String rtn = null;
		   for (String paramName:paramNames) {
			   String paramValue = GetParameterValue(scan,paramName);
			   if (paramValue != null) {
				   if (rtn==null) rtn="";
				   rtn = rtn + "\\" + paramValue;
			   }
		   }
		   return rtn;
	   }
	   
	   private static String GetParameterValue(XnatMrscandataBean scan, String paramName) {
		   String rtn = null;
	       List<XnatAddfieldI> addList = scan.getParameters_addparam();
	       for (XnatAddfieldI addParam : addList) {
	           if (addParam.getName() == null || addParam.getAddfield() == null) {
	              continue;
	           }
	           if (addParam.getName().equals(paramName)) {
	        	   rtn = addParam.getAddfield();
	        	   break;
	           }
	       }
	       if (rtn != null) {
	    	   String parameterValue = rtn;
	    	   rtn="";
	    	   String[] pieces = parameterValue.split("\\\\");
	    	   for (int i=0; i<pieces.length; i++) {
		    	   //3 significant digits after the decimal
		    	   BigDecimal bd = new BigDecimal(new Double(pieces[i]).toString());
		    	   bd = bd.setScale(3, BigDecimal.ROUND_HALF_UP);
		    	   if (i==0)
		    		   rtn+= bd.toString();
		    	   else
		    		   rtn+="\\" + bd.toString();
	    	   }
	       }
		   return rtn;
	   }
	   
	   
	
/**
As per Keith Jamieson:
2. Yes, the shims should come from the first SBRef for most sessions.  The exception is that for our 3T sessions, the SE fieldmap (Type=FieldMap_SE_EPI) is the first scan after shimming, and that scan doesn't save an SBRef series.  For these sessions, you should pull the shim values from that scan instead.  I don't know exactly how your scripts run, but a more robust alternative might be to do this the other way around.  That is, you could take the shim values from the Localizer (ie: the tune-up shims), and look for the first scan whose shims DO NOT match these, as those should be the first valid shims.  This would work whether the first scan is SBRef or not.
3. Ref voltage, shim currents, and slice normals should definitely match for all scans.  The slice positioning will actually be different for BOLD+SE scans and diffusion, because they have a different number of slices.
**/

	private static Hashtable<Integer, Integer> GetReferenceFMRIBlockIndices(List<XnatImagescandataI> scans) {
		Hashtable<Integer, Integer> referenceFMRIBlocks = new Hashtable<Integer, Integer>();
		ArrayList<Integer> startFMRIIndices = new ArrayList<Integer>();
		ArrayList<Integer> localizerIndices = new ArrayList<Integer>();
		if (scans != null && scans.size() > 0) {
			int i=0;
			while (i<scans.size()) {
				if (scans.get(i).getType().equalsIgnoreCase(LOCALIZER_TYPE)) {
					localizerIndices.add(i);
					String localizerShim = GetParameterValue((XnatMrscandataBean)scans.get(i),SHIM_CURRENT);
					//Look ahead for the nearest FMRI SBRef scan
					for (int j=(i+1); j<scans.size(); j++) {
						XnatMrscandataBean mrScan = (XnatMrscandataBean)scans.get(j);
						String scanShim = GetParameterValue(mrScan,SHIM_CURRENT);
						if (!scanShim.equals(localizerShim)) {
							startFMRIIndices.add(new Integer(j));
							//Skip intermediate scans
							i=j;
							break;
						}
					}
				}
				i++;
			}
			/*
			 * Number of reference Blocks would be same as the number of localizer scans
			 */
			if (localizerIndices.size() == 1) {
				//There was only one localizer. Good Imaging session!
				if (startFMRIIndices.size() > 0) {
					referenceFMRIBlocks.put(new Integer(startFMRIIndices.get(0)), new Integer(scans.size()));
				}
			}else {
				//More than one localizer. Possibility of subject adjustment
				for (i=0; i<localizerIndices.size(); i++) {
					try {
						int endIndex=0;
						if ((i+1)<localizerIndices.size()) {
							endIndex=localizerIndices.get(i+1);
						}else{
							endIndex=scans.size();
						}
						referenceFMRIBlocks.put(new Integer(startFMRIIndices.get(i)), new Integer(endIndex));
					}catch(IndexOutOfBoundsException iobe){
						iobe.printStackTrace();
					}
				}
			}
		}
		return referenceFMRIBlocks;
	}

	public static boolean CheckShim(String host, String username, String password, String xnatId) {
		boolean result = false;
		init();

    	SessionManager sessionManager = SessionManager.GetInstance(host, username, password);
		XnatImagesessiondataBean imageSession = GetImageSession(host,username,password,xnatId);
		result = IsShimValid(imageSession);
		try {
			sessionManager.deleteJSESSION();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static boolean CheckShim(String pathToXML) {
		boolean result = false;
		init();

		try {
			XnatImagesessiondataBean imageSession = GetImageSession(pathToXML);
			if (imageSession.getLabel().endsWith(suffix_7T)) {
				is7T = true;
			}
			scanTypesToSkipGlobalPositionCheck = new Hashtable<String,Integer>();
			if (is7T) {
				scanTypesToSkipGlobalPositionCheck.put(DIFFUSION_SCAN_TYPE,1);
				//scanTypesToSkipGlobalPositionCheck.put(DIFFUSION_SBREF_SCAN_TYPE,1);
				scanTypesToSkipGlobalPositionCheck.put(FIELDMAP_DIFFUSION_SCAN_TYPE,1);

			}else {
				scanTypesToSkipGlobalPositionCheck.put(T1W_SCAN_TYPE,1);
				scanTypesToSkipGlobalPositionCheck.put(T2W_SCAN_TYPE,1);
				scanTypesToSkipGlobalPositionCheck.put(DIFFUSION_SCAN_TYPE,1);
				//scanTypesToSkipGlobalPositionCheck.put(DIFFUSION_SBREF_SCAN_TYPE,1);
			}
			result = IsShimValid(imageSession);
		}catch(Exception e) {
			e.printStackTrace();
		}

		return result;
	}


	public static void main(String args[]) {
		String xmlPath=args[0];
		System.out.println("Reading "  + xmlPath);
		boolean shimCheck = CheckShim(xmlPath);
		System.out.println("Shim Check " + shimCheck);
		System.exit(0);
	}



}
