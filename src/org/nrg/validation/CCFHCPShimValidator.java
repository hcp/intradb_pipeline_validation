package org.nrg.validation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.Hashtable;
//import java.util.Iterator;
import java.util.List;
import java.util.Map;
//import java.util.Set;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.nrg.xdat.model.XnatAddfieldI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatMrscandataI;
//import org.nrg.xdat.om.XnatMrscandata;
import org.xml.sax.SAXException;

public class CCFHCPShimValidator extends HCPSessionValidatorNew {
	
	private final static String SETTER_MOSAIC_IMAGETYPE="MOSAIC";
	private Map<String,Boolean> _imageCommentsResults = new HashMap<>();;
	private Map<String,List<String>> _imageCommentsDetails = new HashMap<>();
	private XnatImagesessiondataBean _imageSession;
	private List<XnatImagescandataI> _imageScans;
	
	final Map<String,String> advancedShimCheckFailures = new HashMap<>();
	final Map<String,String> advancedShimCheckSuccesses = new HashMap<>();
	final Map<String,String> shimCheckFailures = new HashMap<>();
	final Map<String,String> shimCheckSuccesses = new HashMap<>();
	final Map<String,String> referenceVoltageCheckFailures = new HashMap<>();
	final Map<String,String> referenceVoltageCheckSuccesses = new HashMap<>();
	boolean fieldmapFollowsLocalizer = true;
	String fieldmapFollowsLocalizerText = "";
	
	public CCFHCPShimValidator(String pathToXML) {
		super();
		try {
			_imageSession = GetImageSession(pathToXML);
			_imageScans = SortByAcquisitionTime(_imageSession);
			String shimCompare = "";
			String shimReference = "";
			String advancedShimCompare = "";
			String advancedShimReference = "";
			String referenceVoltageCompare = "";
			String referenceVoltageReference = "";
			boolean followChecked = true;
			String recentLocalizer = "";
			for (final XnatImagescandataI scan : _imageScans) {
				if (isLocalizer(scan)) {
					shimCompare = "";
					shimReference = "";
					advancedShimCompare = "";
					advancedShimReference = "";
					referenceVoltageCompare = "";
					referenceVoltageReference = "";
					if (scan.getId() != null) {
						followChecked = false;
						recentLocalizer = scan.getId();
					}
				} else if (isSkippedScan(scan)) {
					// Do nothing
				} else {
					final String scanId = scan.getId();
					if (!followChecked && !recentLocalizer.isEmpty()) {
						followChecked = true;
						if (!isFieldmapScan(scan)) {
							fieldmapFollowsLocalizer = false;
							fieldmapFollowsLocalizerText = fieldmapFollowsLocalizerText + 
									((fieldmapFollowsLocalizerText.length()>0) ? "," : "") + recentLocalizer;
						}
					}
					if (referenceVoltageCompare.isEmpty()) {
						referenceVoltageCompare = this.GetReferenceVoltageString(scan); 
						referenceVoltageReference = scan.getId();
						final String result = "Expected:  " + referenceVoltageCompare + " (SCAN=" + referenceVoltageReference +
								") Found: " + referenceVoltageCompare;
						referenceVoltageCheckSuccesses.put(scanId, result); 
					} else {
						final String voltageCheck = GetReferenceVoltageString(scan);
						final String result = "Expected:  " + referenceVoltageCompare + " (SCAN=" + referenceVoltageReference + ") Found: " + voltageCheck;
						if (referenceVoltageCompare.equals(voltageCheck)) {
							referenceVoltageCheckSuccesses.put(scanId, result); 
						} else {
							referenceVoltageCheckFailures.put(scanId, result); 
						}
					}
					if (!isPcaslScan(scan)) {
						if (shimCompare.isEmpty()) {
							shimCompare = this.GetShimCheckString(scan); 
							shimReference = scanId;
							final String result = "Expected:  " + shimCompare + " (SCAN=" + shimReference + ") Found: " + shimCompare;
							shimCheckSuccesses.put(scanId, result); 
						} else {
							final String shimCheck = GetShimCheckString(scan);
							final String result = "Expected:  " + shimCompare + " (SCAN=" + shimReference + ") Found: " + shimCheck;
							if (shimCompare.equals(shimCheck)) {
								shimCheckSuccesses.put(scanId, result); 
							} else {
								shimCheckFailures.put(scanId, result); 
							}
						}
					} else {
						if (advancedShimCompare.isEmpty()) {
							advancedShimCompare = this.GetShimCheckString(scan); 
							advancedShimReference = scanId;
							final String result = "Expected:  " + advancedShimCompare + " (SCAN=" + advancedShimReference + ") Found: " + advancedShimCompare;
							advancedShimCheckSuccesses.put(scanId, result); 
						} else {
							final String shimCheck = GetShimCheckString(scan);
							final String result = "Expected:  " + advancedShimCompare + " (SCAN=" + advancedShimReference + ") Found: " + shimCheck;
							if (advancedShimCompare.equals(shimCheck)) {
								advancedShimCheckSuccesses.put(scanId, result); 
							} else {
								advancedShimCheckFailures.put(scanId, result); 
							}
						}
					}
				}
			}
		} catch (SAXException | IOException e) {
			System.out.println("ERROR:  Couldn't obtain image session:");
			System.out.println(ExceptionUtils.getFullStackTrace(e));
		}
	}

	public Boolean CheckFieldmapFollowsLocalizer() {
		return this.fieldmapFollowsLocalizer;
	}

	public String CheckFieldmapFollowsLocalizerText() { 
		return "<![CDATA[" +
				((this.fieldmapFollowsLocalizer) ? "<font class='status fieldpass'> PASS </font>" : 
					("<font class='status fieldfail'> FAIL </font> (Localizer block" + ((fieldmapFollowsLocalizerText.contains(",")) ? "s" : "") + 
							" not followed by FieldMaps for Localizer scan" +
							((fieldmapFollowsLocalizerText.contains(",")) ? "s" : "") + 
							":  " + fieldmapFollowsLocalizerText + ")"))
				+ "]]>";
	}

	public Boolean CheckShimStandardMode(String scanId) {
		return !shimCheckFailures.containsKey(scanId);
	}

	public String CheckShimStandardModeText(String scanId) {
		final String rtn = (shimCheckSuccesses.containsKey(scanId)) ?
				shimCheckSuccesses.get(scanId) : shimCheckFailures.get(scanId);
		return (rtn != null) ? rtn : " ";
	}

	public Boolean CheckShimAdvancedMode(String scanId) {
		return !advancedShimCheckFailures.containsKey(scanId);
	}
	

	public String CheckShimAdvancedModeText(String scanId) {
		final String rtn = (advancedShimCheckSuccesses.containsKey(scanId)) ?
				advancedShimCheckSuccesses.get(scanId) : advancedShimCheckFailures.get(scanId);
		return (rtn != null) ? rtn : " ";
	}

	public Boolean CheckReferenceVoltage(String scanId) {
		return !referenceVoltageCheckFailures.containsKey(scanId);
	}

	public String CheckReferenceVoltageText(String scanId) {
		final String rtn = (referenceVoltageCheckSuccesses.containsKey(scanId)) ?
				referenceVoltageCheckSuccesses.get(scanId) : referenceVoltageCheckFailures.get(scanId);
		return (rtn != null) ? rtn : " ";
	}
	
	private boolean isFieldmapScan(XnatImagescandataI scan) {
		return scan.getType().toUpperCase().contains("FIELDMAP");
	}
	
	public Boolean CheckImageComments(String scanId) {
		final List<String> commentsText = new ArrayList<>();
		Boolean isOk = true;
		StringBuilder sb = new StringBuilder("Comment(s) contains invalid characters - (");
		final XnatImagescandataI imageScan = getScan(scanId);
		if (imageScan == null || !(imageScan instanceof XnatMrscandataBean)) {
			// We'll just call it OK
			return true;
		}
		if (imageScan instanceof XnatMrscandataBean) {
			final XnatMrscandataBean mrScan = (XnatMrscandataBean) imageScan;
			for (final XnatAddfieldI addParam : mrScan.getParameters_addparam()) {
				final String name = addParam.getName();
				if (!name.contains("Image Comments")) {
					continue;
				}
				final String value = addParam.getAddfield();
				if (value != null && value.contains("?")) {
					isOk = false;
					if (sb.toString().matches(".*\\d+.*")) {
						sb.append(",");
					}
					sb.append(name.replaceAll("[^\\d.]",""));
				}
			}
		}
		if (!isOk) {
			sb.append(")");
			commentsText.add(sb.toString());
		}
		_imageCommentsDetails.put(scanId, commentsText);
		_imageCommentsResults.put(scanId, isOk);
		return isOk;
	}

	public String CheckImageCommentsText(String scanId) {
		
		if (!_imageCommentsDetails.containsKey(scanId)) {
			CheckImageComments(scanId);
		}
		final List<String> details = _imageCommentsDetails.get(scanId);
		return (!details.isEmpty()) ? details.get(0) : "";
		
	}
	
	private XnatImagescandataI getScan(String scanId) {
		for (final XnatImagescandataI scan : _imageScans) {
			if (scan.getId().equals(scanId)) {
				return scan;
			}
		}
		return null;
	}
	

	 private String GetShimCheckString(XnatImagescandataI scan) {
		   return GetShimCheckString((XnatMrscandataBean)scan);
	   }

	 private String GetReferenceVoltageString(XnatImagescandataI scan) {
		   return GetReferenceVoltageString((XnatMrscandataBean)scan);
	   }

	   private String GetShimCheckString(XnatMrscandataBean scan) {
		   String shim_compare = null;

	       String shim_shimcurr = "";
	       String lOffset = "";

		       List<XnatAddfieldI> addList = scan.getParameters_addparam();
		       for (XnatAddfieldI addParam : addList) {
		           if (addParam.getName() == null || addParam.getAddfield() == null) {
		              continue;
		           }
		           if (addParam.getName().equals("Siemens GRADSPEC alShimCurrent")) {
		               shim_shimcurr = addParam.getAddfield();
		           } else  if (addParam.getName().equals("Siemens GRADSPEC lOffset")) {
		        	   lOffset = addParam.getAddfield();
		           }
		       }
		       shim_compare = lOffset + "\\" + shim_shimcurr ;
		   return shim_compare;
	   }

	   private String GetReferenceVoltageString(XnatMrscandataBean scan) {
		   String shim_compare = null;

	       String refVol = "";

		       List<XnatAddfieldI> addList = scan.getParameters_addparam();
		       for (XnatAddfieldI addParam : addList) {
		           if (addParam.getName() == null || addParam.getAddfield() == null) {
		              continue;
		           }
		           if (addParam.getName().equals("Siemens txRefAmp")) {
		               refVol = addParam.getAddfield();
		           }
		       }
		           shim_compare = refVol ;
		   return shim_compare;
	   }

	private boolean isLocalizer(XnatImagescandataI scan) {
		final String seriesDesc = scan.getSeriesDescription().toUpperCase();
		return seriesDesc.contains("LOCALIZER") && !seriesDesc.contains("_ALIGNED");
	}

	private boolean isSkippedScan(XnatImagescandataI scan) {
		final String seriesDesc = scan.getSeriesDescription().toUpperCase();
		return seriesDesc.contains("LOCALIZER_ALIGNED") || seriesDesc.contains("AAHSCOUT") || seriesDesc.contains("POSDISP") || 
				(seriesDesc.contains("_SETTER") && !isMosaicType(scan)); 
	}

	private boolean isMosaicType(XnatImagescandataI scan) {
		if (scan instanceof XnatMrscandataI) {
			return ((XnatMrscandataI)scan).getParameters_imagetype().toUpperCase().endsWith(SETTER_MOSAIC_IMAGETYPE);
		}
		return false;
	}

	private boolean isPcaslScan(XnatImagescandataI scan) {
		return scan.getSeriesDescription().toUpperCase().contains("PCASL");
	}

}
