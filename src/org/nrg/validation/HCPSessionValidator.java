 /**
 * Copyright (c) 2013 Washington University School of Medicine
 */

package org.nrg.validation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.nrg.xdat.bean.reader.XDATXMLReader;
import org.nrg.xdat.model.XnatAddfieldI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrscandata;
import org.nrg.xnattools.xml.XMLSearch;
import org.xml.sax.SAXException;

/**
 * @author Mohana Ramaratnam (mohanakannan9@gmail.com)
 *
 */
public class HCPSessionValidator {

	protected final static String MOSAIC_TYPE="MOSAIC";


	protected static List<XnatImagescandataI> SortByAcquisitionTime(XnatImagesessiondataBean imageSession) {
		   List<XnatImagescandataI> sortedScans =  imageSession.getScans_scan();
		   List<XnatImagescandataI> mrSortedScans = new ArrayList<XnatImagescandataI>();
		   for (XnatImagescandataI scan: sortedScans) {
			   if (scan.getXSIType().equals(XnatMrscandata.SCHEMA_ELEMENT_NAME) ) {
				   if (null != ((XnatMrscandataBean)scan).getStarttime() )
					   mrSortedScans.add(scan);
			   }
		   }
		   Collections.sort(mrSortedScans,new Comparator<XnatImagescandataI>(){
			    @Override
			    public int compare(XnatImagescandataI s1, XnatImagescandataI s2) {
			        return ((String)s1.getStarttime()).compareTo((String)s2.getStarttime());
			}});
		   return mrSortedScans;
	}
	
	
		
/* As per email from Keith Jamison
 * For the validation algorithm, starting with the REST[1..4]_SBRef scan in each session, all scans should have identical values for the following items in the "Siemens Extra Info" block (0029 1020):
 * 1. shim currents: sGRADSPEC.alShimCurrent[0..4]
 * 2. slice[0] positioning: sSliceArray.asSlice[0].sPosition.(dSag|dCor|dTra) and sSliceArray.asSlice[0].sNormal.(dSag|dCor|dTra)
 * 3. reference voltage: asNucleusInfo[0].flReferenceAmplitude
 */

	protected static boolean CheckShimCurrentInTheBlock(Integer startIndexInt, Integer endIndexInt, List<XnatImagescandataI> scans) {
	   boolean isValidShim = true;
	   int startIndex = startIndexInt.intValue();
	   int endIndex = endIndexInt.intValue();
	   try {
		   XnatMrscandataBean referenceFMRIScan = (XnatMrscandataBean)scans.get(startIndex);
		   String referenceShim = GetShimCheckString(referenceFMRIScan);
		   for (int i=startIndex+1; i<endIndex; i++) {
			   String shim_compare=GetShimCheckString(scans.get(i));
			   if (referenceShim!=null && !referenceShim.equals(shim_compare)) {
				   System.out.println(" Shim checks for scan " + referenceFMRIScan.getId() + " doesnt match " + scans.get(i).getId());
				   System.out.println("Scan " + referenceFMRIScan.getId() + " : " + referenceShim);
				   System.out.println("Scan " + scans.get(i).getId() + " : " + shim_compare);
				   isValidShim=false;
				   break;
			   }
		   }
	   }catch(IndexOutOfBoundsException iobe) {
		   isValidShim=false;
	   }
	   return isValidShim;
   }

   private static String GetShimCheckString(XnatImagescandataI scan) {
	   return GetShimCheckString((XnatMrscandataBean)scan);
   }
   
   private static String GetShimCheckString(XnatMrscandataBean scan) {
	   String shim_compare = null;
       String shim_sposition_dSag = "";
       String shim_sposition_dTra = "";
       String shim_sposition_dCor = "";
       String shim_snormal_dSag = "";
       String shim_snormal_dTra = "";
       String shim_snormal_dCor = "";

       String shim_shimcurr = "";
       String shim_transmitter_reference_voltage ="";

    	   
	       List<XnatAddfieldI> addList = scan.getParameters_addparam();
	       for (XnatAddfieldI addParam : addList) {
	           if (addParam.getName() == null || addParam.getAddfield() == null) {
	              continue;
	           }
	           if (addParam.getName().equals("Siemens GRADSPEC alShimCurrent")) {
	               shim_shimcurr = addParam.getAddfield();
	           } else  if (addParam.getName().equals("Siemens sSliceArray.asSlice[0].sPosition.dSag")) {
	               shim_sposition_dSag = addParam.getAddfield();
	           } else  if (addParam.getName().equals("Siemens sSliceArray.asSlice[0].sPosition.dCor")) {
	        	   shim_sposition_dCor = addParam.getAddfield();
	           } else  if (addParam.getName().equals("Siemens sSliceArray.asSlice[0].sPosition.dTra")) {
	        	   shim_sposition_dTra = addParam.getAddfield();
		       } else  if (addParam.getName().equals("Siemens txRefAmp")) {
		    	   shim_transmitter_reference_voltage = addParam.getAddfield();
	           } else  if (addParam.getName().equals("Siemens sSliceArray.asSlice[0].sNormal.dSag")) {
	               shim_snormal_dSag = addParam.getAddfield();
	           } else  if (addParam.getName().equals("Siemens sSliceArray.asSlice[0].sNormal.dCor")) {
	        	   shim_snormal_dCor = addParam.getAddfield();
	           } else  if (addParam.getName().equals("Siemens sSliceArray.asSlice[0].sNormal.dTra")) {
	        	   shim_snormal_dTra = addParam.getAddfield();
	           }
	       }
	           shim_compare = shim_shimcurr + shim_sposition_dSag + shim_sposition_dCor + shim_sposition_dTra + shim_snormal_dSag + shim_snormal_dCor + shim_snormal_dTra + shim_transmitter_reference_voltage;
	   return shim_compare;
   }
   
   protected static XnatImagesessiondataBean GetImageSession(String host, String username, String password, String xnat_id) {
	   XnatImagesessiondataBean imageSession = null;
	   try {
	        XMLSearch search = new XMLSearch(host,username,password);
	       	imageSession  = (XnatImagesessiondataBean) search.getBeanFromHost(xnat_id, true);
	        System.out.println(imageSession.getId() + " " + imageSession.getAcquisitionSite());
       }catch(Exception e) {
    	   e.printStackTrace();
       }
	   return imageSession;
   }

   protected static XnatImagesessiondataBean GetImageSession(String pathToXML) throws FileNotFoundException, SAXException, IOException{
	   XnatImagesessiondataBean imageSession = null;
       File f = new File(pathToXML);
       InputStream fis = new FileInputStream(f);
       XDATXMLReader reader = new XDATXMLReader();
       org.nrg.xdat.bean.base.BaseElement base = reader.parse(fis);
       try {
    	   fis.close();
       }catch(Exception e){}
	   return (XnatImagesessiondataBean)base;
   }

   
   
}
